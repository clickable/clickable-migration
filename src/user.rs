use std::fmt::Display;
use std::io::Write;

pub fn ask(msg: &str, default: Option<&str>) -> String {
    print!("{} ", msg);
    if let Some(default) = default {
        print!("[{}] ", default);
    }
    std::io::stdout().flush().expect(ERROR_CLI);

    let mut input = String::new();
    std::io::stdin().read_line(&mut input).expect(ERROR_CLI);
    let choice = input.trim();

    if choice.is_empty() {
        if let Some(default) = default {
            return default.to_string();
        }
    }

    choice.to_string()
}

pub fn confirm(msg: &str) -> bool {
    let choice = ask(msg, Some("Y")).to_lowercase();

    choice == "y" || choice == "yes"
}

pub fn select<T: Display + Copy>(msg: &str, options: &[T]) -> T {
    if options.is_empty() {
        panic!("Empty list of options passed to command line select");
    }

    loop {
        println!("{}", msg);
        for (idx, opt) in options.iter().enumerate() {
            println!("{}: {}", idx, opt);
        }

        print!("\nChoose a number [0]: ");
        std::io::stdout().flush().expect(ERROR_CLI);

        let mut input = String::new();
        std::io::stdin().read_line(&mut input).expect(ERROR_CLI);
        let choice = input.trim();

        println!();

        if choice.is_empty() {
            return options[0];
        }

        match choice.parse::<usize>() {
            Ok(val) if val < options.len() => return options[val],
            _ => println!("\nChoose one of the options listed!\n"),
        }
    }
}

static ERROR_CLI: &str = "Failed interact with command line";
