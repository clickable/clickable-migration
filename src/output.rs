use std::fmt::{self, Display};
use std::fs::File;
use std::io::{BufWriter, Write};
use std::path::Path;

use crate::{user, EmptyResult};

#[derive(Clone, Copy, PartialEq)]
enum SavingAction {
    Nothing,
    Overwrite,
    NewFile,
}

impl Display for SavingAction {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Nothing => write!(f, "Leave unchanged"),
            Self::Overwrite => write!(f, "Overwrite the config with selected changes"),
            Self::NewFile => write!(f, "Save migrated config to a new file"),
        }
    }
}

pub fn save(path: &Path, alt_name: &str, content: &str, confirm: bool) -> EmptyResult {
    let path = if confirm {
        match user::select(
            &format!("Do you want to save the changes to {:?}?", path),
            &[
                SavingAction::Overwrite,
                SavingAction::NewFile,
                SavingAction::Nothing,
            ],
        ) {
            SavingAction::Nothing => {
                return Ok(());
            }
            SavingAction::NewFile => {
                let mut path = path
                    .parent()
                    .expect("Failed to construct file path.")
                    .to_path_buf();
                path.push(alt_name);
                path
            }

            SavingAction::Overwrite => path.to_path_buf(),
        }
    } else {
        path.to_path_buf()
    };

    eprintln!("Saving config to {:?}", path);

    let file = File::create(path)?;
    let mut writer = BufWriter::new(file);
    writer.write_all(content.as_bytes())?;

    Ok(())
}
