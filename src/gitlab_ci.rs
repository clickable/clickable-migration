use serde::Deserialize;
use serde_yaml::Value;
use std::collections::HashMap;
use std::fs::File;
use std::io::{BufReader, Read};
use std::path::{Path, PathBuf};

use crate::{output, EmptyResult, Result};

#[derive(Deserialize, Debug)]
struct GitlabCiConfig {
    #[serde(flatten)]
    jobs: HashMap<String, Value>,
}

pub struct GitlabCiConfigCheck {
    config: GitlabCiConfig,
    path: PathBuf,
    dst: Option<PathBuf>,
    content: String,
    changed: bool,
    confirm: bool,
}

impl GitlabCiConfig {
    fn load<P: AsRef<Path>>(path: &P) -> Result<Self> {
        let file = File::open(path)?;
        let reader = BufReader::new(file);
        Ok(serde_yaml::from_reader(reader)?)
    }
}

impl GitlabCiConfigCheck {
    pub fn new(path: PathBuf, dst: Option<PathBuf>, confirm: bool) -> Result<Self> {
        let config = GitlabCiConfig::load(&path)?;

        let mut file = File::open(&path)?;
        let mut content = String::new();
        file.read_to_string(&mut content)?;

        Ok(Self {
            config,
            path,
            dst,
            content,
            changed: false,
            confirm,
        })
    }

    /// Returns `true` if the file has been migrated and `false` if there was nothing to do
    pub fn migrate(mut self) -> Result<bool> {
        self.check_scripts();

        if self.changed {
            self.save()?;
            Ok(true)
        } else {
            Ok(false)
        }
    }

    fn save(&self) -> EmptyResult {
        output::save(
            self.dst.as_ref().unwrap_or(&self.path),
            ".gitlab-ci-migrated.yml",
            &self.content,
            self.confirm,
        )
    }

    fn check_scripts(&mut self) {
        for job in &mut self.config.jobs.values_mut() {
            if let Value::Mapping(job) = job {
                if let Some(mut script) = job.get_mut(&Value::String("script".to_string())) {
                    if check_script(&mut script, &mut self.content) {
                        self.changed = true;
                    }
                }
            }
        }
    }
}

fn check_script(script: &mut Value, content: &mut String) -> bool {
    match script {
        Value::String(string) => check_command(string, content),
        Value::Sequence(seq) => {
            let mut changed = false;
            for script in seq {
                if check_script(script, content) {
                    changed = true;
                }
            }
            changed
        }
        _ => false,
    }
}

fn check_command(command: &mut String, content: &mut String) -> bool {
    if !is_clickable_call(command) {
        return false;
    }
    let mut changed = false;
    changed |= check_chaining(command, content);
    changed |= check_clean_build(command, content);
    changed |= check_build_libs(command, content);
    changed |= check_clean_libs(command, content);
    changed |= check_test_libs(command, content);
    changed |= check_click_build(command, content);
    changed |= check_flags_before_sub_command(command, content);
    changed |= check_flags_in_chain_command(command, content);
    changed
}

fn check_chaining(command: &mut String, content: &mut String) -> bool {
    if count_sub_commands(command) > 1 {
        let new = command.replace("clickable ", "clickable chain ");
        replace(command, content, &new)
    } else {
        false
    }
}

fn check_clean_build(command: &mut String, content: &mut String) -> bool {
    if contains_sub_command(command, "clean-build") {
        let new = command.replace("clean-build", "build --clean");
        replace(command, content, &new)
    } else {
        false
    }
}

fn check_build_libs(command: &mut String, content: &mut String) -> bool {
    if contains_sub_command(command, "build-libs") {
        let new = command.replace("build-libs", "build --libs");
        replace(command, content, &new)
    } else {
        false
    }
}

fn check_clean_libs(command: &mut String, content: &mut String) -> bool {
    if contains_sub_command(command, "clean-libs") {
        let new = command.replace("clean-libs", "clean --libs");
        replace(command, content, &new)
    } else {
        false
    }
}

fn check_test_libs(command: &mut String, content: &mut String) -> bool {
    if contains_sub_command(command, "test-libs") {
        let new = command.replace("test-libs", "test --libs");
        replace(command, content, &new)
    } else {
        false
    }
}

fn check_click_build(command: &mut String, content: &mut String) -> bool {
    if contains_sub_command(command, "click-build") {
        if count_sub_commands(command) == 1 {
            let new =
                ": # TODO Remove this line. This was a click-build command previously, removed by the Clickable migration tool.".to_string();
            replace(command, content, &new)
        } else {
            let list: Vec<&str> = command.split(' ').filter(|s| *s != "click-build").collect();
            let new = list.join(" ");
            replace(command, content, &new)
        }
    } else {
        false
    }
}

fn check_flags_before_sub_command(command: &mut String, content: &mut String) -> bool {
    if has_flags_prepending_sub_command(command) {
        let mut list: Vec<&str> = command.split(' ').filter(|s| !s.starts_with('-')).collect();
        let mut list_params: Vec<&str> =
            command.split(' ').filter(|s| s.starts_with('-')).collect();
        list.append(&mut list_params);
        let new = list.join(" ");
        replace(command, content, &new)
    } else {
        false
    }
}

fn check_flags_in_chain_command(command: &mut String, content: &mut String) -> bool {
    if contains_sub_command(command, "chain") && has_command_specific_flags(command) {
        let new = separate_sub_commands(command);
        replace(command, content, &new)
    } else {
        false
    }
}

fn replace(command: &mut String, content: &mut String, new: &str) -> bool {
    *content = content.replace(command.as_str(), new);
    *command = command.replace(command.as_str(), new);
    true
}

fn has_command_specific_flags(command: &str) -> bool {
    for p in command.split(' ') {
        if p.starts_with('-')
            && !matches!(
                p,
                "-h" | "--help"
                    | "-c"
                    | "--config"
                    | "-s"
                    | "--serial-number"
                    | "--ssh"
                    | "-a"
                    | "--arch"
                    | "--container-mode"
                    | "--docker-image"
                    | "--nvidia"
                    | "--no-nvidia"
                    | "--non-interactive"
                    | "--verbose"
                    | "--clean"
            )
        {
            return true;
        }
    }
    false
}

fn has_flags_prepending_sub_command(command: &str) -> bool {
    for p in command.split(' ') {
        if is_sub_command(p) {
            return false;
        }
        if p.starts_with('-') {
            return true;
        }
    }
    false
}

fn is_clickable_call(command: &str) -> bool {
    command.split(' ').any(|p| p == "clickable")
}

fn is_sub_command(command: &str) -> bool {
    matches!(
        command,
        "build"
            | "build-libs"
            | "clean"
            | "clean-build"
            | "clean-libs"
            | "click-build"
            | "create"
            | "desktop"
            | "devices"
            | "gdb"
            | "gdbserver"
            | "ide"
            | "install"
            | "launch"
            | "log"
            | "logs"
            | "no-lock"
            | "publish"
            | "review"
            | "run"
            | "screenshots"
            | "setup"
            | "shell"
            | "test"
            | "test-libs"
            | "update"
            | "writable-image"
    )
}

fn contains_sub_command(command: &str, sub_command: &str) -> bool {
    command.split(' ').any(|s| s == sub_command)
}

fn count_sub_commands(command: &str) -> usize {
    command.split(' ').filter(|s| is_sub_command(s)).count()
}

fn push_separate_sub_command(list: &mut Vec<String>, mut sub_command: Vec<&str>) {
    if !sub_command.is_empty() {
        sub_command.insert(0, "clickable");
        list.push(sub_command.join(" "));
    }
}

fn separate_sub_commands(command: &str) -> String {
    let mut list = Vec::new();
    let mut sub_command = Vec::new();

    for p in command.split(' ') {
        if matches!(p, "clickable" | "chain") {
            continue;
        }

        if p == "&&" {
            push_separate_sub_command(&mut list, sub_command);
            sub_command = Vec::new();
            continue;
        }

        if is_sub_command(p) {
            push_separate_sub_command(&mut list, sub_command);
            sub_command = Vec::new();
        }

        sub_command.push(p);
    }

    push_separate_sub_command(&mut list, sub_command);
    list.join(" && ")
}
