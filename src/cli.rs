use std::env;
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(StructOpt)]
pub struct Cli {
    /// Apply changes and save files without confirmation.
    #[structopt(long)]
    pub no_confirm: bool,
    /// Skip Clickable config migration.
    #[structopt(long)]
    pub skip_clickable_json: bool,
    /// Skip Gitlab CI config migration.
    #[structopt(long)]
    pub skip_gitlab_ci: bool,
    #[structopt(long, short)]
    pub project: Option<PathBuf>,
    /// Path to your clickable.json. Defaults to <project>/clickable.json.
    #[structopt(long, short)]
    pub clickable_json: Option<PathBuf>,
    /// Custom destination path for your migrated Clickable config.
    #[structopt(long)]
    pub clickable_json_dst: Option<PathBuf>,
    /// Path to your Gitlab CI config. Defaults to <project>/.gitlab-ci.yml.
    #[structopt(long, short)]
    pub gitlab_ci: Option<PathBuf>,
    /// Custom destination path for your migrated Gitlab CI config.
    #[structopt(long)]
    pub gitlab_ci_dst: Option<PathBuf>,
}

impl Cli {
    pub fn parse_args() -> Self {
        Self::from_args()
    }

    pub fn get_clickable_json_path(&self) -> PathBuf {
        self.get_file(&self.clickable_json, "clickable.json")
    }

    pub fn get_gitlab_config_path(&self) -> PathBuf {
        self.get_file(&self.gitlab_ci, ".gitlab-ci.yml")
    }

    fn get_file(&self, path: &Option<PathBuf>, default_name: &str) -> PathBuf {
        if let Some(path) = path {
            path.clone()
        } else {
            let mut path = if let Some(project) = &self.project {
                project.clone()
            } else {
                env::current_dir().expect("Current working directory is invalid.")
            };
            path.push(default_name);
            path
        }
    }
}
