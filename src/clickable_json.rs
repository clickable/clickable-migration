use regex::{Regex, Replacer};
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;
use std::fmt::{self, Display};
use std::fs::File;
use std::io::BufReader;
use std::path::{Path, PathBuf};

use crate::{output, user, EmptyResult, Result};

#[derive(Clone, Copy, PartialEq)]
enum Action {
    Nothing,
    Remove,
    Rename,
    Replace,
    Change,
}

impl Default for Action {
    fn default() -> Self {
        Self::Nothing
    }
}

#[derive(Default)]
struct Suggestion<'a> {
    key: &'a str,
    default_action: Action,
    change: Option<Value>,
    rename: Option<&'a str>,
    replace: Option<(&'a str, Value)>,
    remove: bool,
}

impl<'a> Suggestion<'a> {
    fn removal(key: &'a str) -> Self {
        Self {
            key,
            default_action: Action::Remove,
            remove: true,
            ..Default::default()
        }
    }

    fn renaming_or_removal(key: &'a str, new: &'a str) -> Self {
        Self {
            key,
            default_action: Action::Rename,
            rename: Some(new),
            remove: true,
            ..Default::default()
        }
    }

    fn renaming(key: &'a str, new: &'a str) -> Self {
        Self {
            key,
            default_action: Action::Rename,
            rename: Some(new),
            ..Default::default()
        }
    }

    fn changing(key: &'a str, value: Value) -> Self {
        Self {
            key,
            default_action: Action::Change,
            change: Some(value),
            ..Default::default()
        }
    }

    fn replacement_or_removal(key: &'a str, replacement: (&'a str, Value)) -> Self {
        Self {
            key,
            default_action: Action::Replace,
            replace: Some(replacement),
            remove: true,
            ..Default::default()
        }
    }
}

impl Display for Action {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Nothing => write!(f, "Leave unchanged"),
            Self::Remove => write!(f, "Remove"),
            Self::Rename => write!(f, "Rename"),
            Self::Replace => write!(f, "Replace"),
            Self::Change => write!(f, "Change"),
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
struct ClickableJson {
    #[serde(flatten)]
    content: HashMap<String, Value>,
}

pub struct ClickableJsonCheck {
    config: ClickableJson,
    path: PathBuf,
    dst: Option<PathBuf>,
    changed: bool,
    confirm: bool,
}

impl ClickableJson {
    fn load<P: AsRef<Path>>(path: &P) -> Result<Self> {
        let file = File::open(path)?;
        let reader = BufReader::new(file);
        Ok(serde_json::from_reader(reader)?)
    }
}

impl ClickableJsonCheck {
    pub fn new(path: PathBuf, dst: Option<PathBuf>, confirm: bool) -> Result<Self> {
        let config = ClickableJson::load(&path)?;

        Ok(Self {
            config,
            path,
            dst,
            changed: false,
            confirm,
        })
    }

    /// Returns `true` if the file has been migrated and `false` if there was nothing to do
    pub fn migrate(mut self) -> Result<bool> {
        self.check_python_builder();
        self.check_dirty();
        self.check_template();
        self.check_dependencies_build();
        self.check_arch();
        self.check_clickable_minimum_required();
        self.check_placeholders();

        if self.changed {
            self.save()?;
            Ok(true)
        } else {
            Ok(false)
        }
    }

    fn save(&self) -> EmptyResult {
        let json = serde_json::to_string_pretty(&self.config)?;
        output::save(
            self.dst.as_ref().unwrap_or(&self.path),
            "clickable-migrated.json",
            &json,
            self.confirm,
        )
    }

    fn suggest_action_with_messages<S: Display>(&mut self, suggestion: Suggestion, messages: &[S]) {
        let key = suggestion.key;

        if self.config.content.contains_key(key) {
            let action = if self.confirm {
                if suggestion.change.is_none() {
                    eprintln!("Found unsupported field '{}' in config file.", key);
                }

                for m in messages {
                    eprintln!("{}", m);
                }

                let mut options = Vec::new();
                if suggestion.rename.is_some() {
                    options.push(Action::Rename);
                }
                if suggestion.replace.is_some() {
                    options.push(Action::Replace);
                }
                if suggestion.change.is_some() {
                    options.push(Action::Change);
                }
                if suggestion.remove {
                    options.push(Action::Remove);
                }
                options.push(Action::Nothing);

                user::select("What do you want to do?", &options)
            } else {
                suggestion.default_action
            };

            if action != Action::Nothing {
                self.changed = true;
            }

            match action {
                Action::Nothing => {}
                Action::Rename => {
                    if let Some(value) = self.config.content.remove(key) {
                        self.config
                            .content
                            .insert(suggestion.rename.unwrap().to_string(), value);
                    }
                }
                Action::Replace => {
                    self.config.content.remove(key);
                    let (key, value) = suggestion.replace.unwrap();
                    self.config.content.insert(key.to_string(), value);
                }
                Action::Remove => {
                    self.config.content.remove(key);
                }
                Action::Change => {
                    self.config
                        .content
                        .insert(key.to_string(), suggestion.change.unwrap());
                }
            }
        }
    }

    fn suggest_action(&mut self, suggestion: Suggestion) {
        let empty = [""; 0];
        self.suggest_action_with_messages(suggestion, &empty);
    }

    fn check_template(&mut self) {
        self.suggest_action(Suggestion::renaming("template", "builder"));
    }

    fn check_dependencies_build(&mut self) {
        self.suggest_action(Suggestion::renaming(
            "dependencies_build",
            "dependencies_host",
        ));
    }

    fn check_python_builder(&mut self) {
        let key = "builder";
        if let Some(Value::String(builder)) = self.config.content.get(key) {
            if builder == "python" {
                let messages =
                    ["The 'python' builder has been replaced by the 'precompiled' builder."];
                self.suggest_action_with_messages(
                    Suggestion::changing(key, Value::String("precompiled".to_string())),
                    &messages,
                )
            }
        }
    }

    fn check_dirty(&mut self) {
        let key = "dirty";
        if let Some(field) = self.config.content.get(key) {
            if let Value::Bool(dirty) = field {
                if *dirty {
                    let messages = [
                        "Dirty builds are now the default behaviour.",
                        "It is recommend to just remove the 'default' field.",
                    ];
                    self.suggest_action_with_messages(Suggestion::removal(key), &messages);
                } else {
                    let messages = [
                        "Dirty builds are now the default behaviour.",
                        "To ensure the build directory is always cleaned before building, \
                            you can replace the field by 'always_clean' set to 'true'.",
                    ];
                    let replacement = ("always_clean", Value::Bool(true));
                    self.suggest_action_with_messages(
                        Suggestion::replacement_or_removal(key, replacement),
                        &messages,
                    );
                }
            } else {
                let messages = [
                    "Dirty builds are now the default behaviour.",
                    "Your 'dirty' field does not even seem to be a valid bool.",
                ];
                self.suggest_action_with_messages(Suggestion::removal(key), &messages);
            }
        }
    }

    fn check_arch(&mut self) {
        let messages = [
            "It is recommended to set the target architecture via command line \
                parameter '--arch' when needed and remove it from the config file.",
            "In case your app is somehow restricted to one architecture in general, \
                renaming the field to 'restrict_arch' is the right choice.",
        ];
        self.suggest_action_with_messages(
            Suggestion::renaming_or_removal("arch", "restrict_arch"),
            &messages,
        );
    }

    fn check_clickable_minimum_required(&mut self) {
        let key = "clickable_minimum_required";
        if let Some(Value::String(minimum)) = self.config.content.get(key) {
            if minimum.starts_with('7') {
                return;
            }
        }

        let messages =
            ["It is recommended to set the 'clickable_minimum_required' at least to '7'"];
        self.suggest_action_with_messages(
            Suggestion::changing(key, Value::String("7".to_string())),
            &messages,
        );
    }

    fn check_placeholders(&mut self) {
        for (k, v) in &mut self.config.content {
            if check_value_for_placeholders(k, v, self.confirm) {
                self.changed = true;
            }
        }
    }
}

fn check_value_for_placeholders(key: &str, value: &mut Value, confirm: bool) -> bool {
    let mut matched: bool = false;
    match value {
        Value::String(string) => {
            if check_string_for_placeholders(key, string, confirm) {
                matched = true
            }
        }
        Value::Array(vec) => {
            for mut v in vec {
                if check_value_for_placeholders(key, &mut v, confirm) {
                    matched = true;
                }
            }
        }
        Value::Object(obj) => {
            for (k, mut v) in obj {
                if check_value_for_placeholders(k, &mut v, confirm) {
                    matched = true;
                }
            }
        }
        _ => {}
    }
    matched
}

fn check_replacement<R: Replacer>(
    reg: &Regex,
    mut rep: R,
    reason: &str,
    key: &str,
    value: &mut String,
    confirm: bool,
) -> bool {
    if reg.is_match(value) {
        let new = reg.replace_all(value, rep.by_ref()).into_owned();
        let confirmed = !confirm
            || user::confirm(&format!(
                "Unsupported placeholders detected ({}). \
                Do you want to replace\n  '{}'\nby\n  '{}'\nin '{}'?",
                reason, value, new, key
            ));

        if confirmed {
            *value = new;
            return true;
        }
    }
    false
}

pub fn check_string_for_placeholders(key: &str, value: &mut String, confirm: bool) -> bool {
    lazy_static::lazy_static! {
        static ref RE_BRACES: Regex = Regex::new(r"\$(?P<e>[\w_]+)").unwrap();
        static ref RE_CASE: Regex = Regex::new(r"\$\{[\w_]*[[:lower:]]+[\w_]*\}").unwrap();
    }

    let mut matches = false;

    if check_replacement(
        &RE_BRACES,
        "$${$e}",
        "missing curly braces",
        key,
        value,
        confirm,
    ) {
        matches = true;
    }

    let rep = |caps: &regex::Captures| caps[0].to_uppercase();
    if check_replacement(&RE_CASE, rep, "lower case letters", key, value, confirm) {
        matches = true;
    }

    matches
}
