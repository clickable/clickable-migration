pub mod cli;
pub mod clickable_json;
pub mod gitlab_ci;
pub mod output;
pub mod user;

use cli::Cli;
use clickable_json::ClickableJsonCheck;
use gitlab_ci::GitlabCiConfigCheck;
use std::error::Error;

type Result<T> = std::result::Result<T, Box<dyn Error>>;
type EmptyResult = Result<()>;

fn main() {
    println!("Going to migrate your project to Clickable 7.");

    let args = Cli::parse_args();

    let clickable_path = args.get_clickable_json_path();
    let gitlab_path = args.get_gitlab_config_path();

    let mut migrated = false;

    if !args.skip_clickable_json {
        if clickable_path.exists() {
            if ClickableJsonCheck::new(clickable_path, args.clickable_json_dst, !args.no_confirm)
                .expect("Failed to load Clickable config.")
                .migrate()
                .expect("Failed to save changes to Clickable config.")
            {
                migrated = true;
            }
        } else {
            eprintln!(
                "Clickable config {:?} does not exist. Skipping...",
                clickable_path
            );
        }
    }
    if !args.skip_gitlab_ci {
        if gitlab_path.exists() {
            if GitlabCiConfigCheck::new(gitlab_path, args.gitlab_ci_dst, !args.no_confirm)
                .expect("Failed to load Gitlab CI config.")
                .migrate()
                .expect("Failed to save changes to Gitlab CI config.")
            {
                migrated = true;
            }
        } else {
            eprintln!(
                "Gitlab CI config {:?} does not exist. Skipping...",
                gitlab_path
            );
        }
    }

    if migrated {
        eprintln!("Migration finished.");
    } else {
        eprintln!("Nothing to do. Finally a day off for a little tool...");
    }
}
