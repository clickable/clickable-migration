# Development

## Cross Compiling

Make sure you have the C cross compilers installed, e.g. on Ubuntu:

```bash
sudo apt install gcc-aarch64-linux-gnu gcc-arm-linux-gnueabihf
```

Enable your Rust tool chain for cross compiling:

```bash
rustup target add aarch64-unknown-linux-gnu
rustup target add armv7-unknown-linux-gnueabihf
```

Build the tool for foreign targets:

```bash
cargo build --target armv7-unknown-linux-gnueabihf
cargo build --target aarch64-unknown-linux-gnu
```

Find the binaries inside the `target` folder.
