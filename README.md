# Clickable Migration Tool

This tool is meant to migrate your Clickable 6 project to
[Clickable 7](https://clickable-ut.dev/en/latest/). It covers
the [Clickable config](https://clickable-ut.dev/en/latest/clickable-json.html)
and a possible Gitlab CI config (`.gitlab-ci.yml`).

## Usage

Run a full and interactive migration in your project directory:

```bash
clickable-migration
```

Print the help message to learn about all features:

```bash
clickable-migration --help
```

## Installation

The Clickable Migration Tool is available as a
[crate](https://crates.io/crates/clickable-migration). Make sure you have
the [Rust tool chain](https://www.rust-lang.org/tools/install)
installed. Then install with:

```bash
cargo install clickable-migration
```
